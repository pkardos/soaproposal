cmake_minimum_required(VERSION 3.13)
project(SoaProposal)

set(CMAKE_CXX_STANDARD 17)

add_executable(SoaProposal main.cpp)