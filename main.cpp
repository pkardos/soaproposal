#include <iostream>
#include <type_traits>
#include <atomic>
#include <vector>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <cassert>


class CoherentSet {
public:
	CoherentSet() : m_id(generator.fetch_add(1)) {}

	bool operator==(const CoherentSet& rhs) const {
		return m_id == rhs.m_id;
	}
	bool operator!=(const CoherentSet& rhs) const {
		return !operator==(rhs);
	}
private:
	uint64_t m_id;
	inline static std::atomic_uint64_t generator;
};



template <class ContainerT>
class SelectableContainer : public ContainerT {
public:
	using ContainerT::ContainerT;

	template <class... Args>
	SelectableContainer(CoherentSet coherentSet, Args&&... args) : m_coherentSet(coherentSet), ContainerT(std::forward<Args>(args)...) {}

	CoherentSet GetCoherentSet() const {
		return m_coherentSet;
	}
private:
	CoherentSet m_coherentSet;
};



class SelectionSet {
public:
	template <class UnderlyingT, class Predicate>
	SelectionSet(const SelectableContainer<UnderlyingT>& container, Predicate predicate) {
		size_t index = 0;
		for (const auto& item : container) {
			if (predicate(item)) {
				m_selectedIndices.push_back(index);
			}
			++index;
		}

		m_coherentSet = container.GetCoherentSet();
	}

	size_t operator[](size_t index) const {
		return m_selectedIndices[index];
	}

	size_t Size() const {
		return m_selectedIndices.size();
	}

	CoherentSet GetCoherentSet() const {
		return m_coherentSet;
	}
private:
	std::vector<size_t> m_selectedIndices;
	CoherentSet m_coherentSet;
};



template <class SelectableContainerT>
class SelectionView;

template <class UnderlyingT>
class SelectionView<SelectableContainer<UnderlyingT>> {
public:
	using IteratorT = std::decay_t<decltype(((SelectableContainer<UnderlyingT>*)nullptr)->begin())>;
	using ConstIteratorT = std::decay_t<decltype(((SelectableContainer<UnderlyingT>*)nullptr)->cbegin())>;

	SelectionView(const SelectionSet& selection, const SelectableContainer<UnderlyingT>& container)
		: m_selection(selection), m_container(container)
	{
		if (selection.GetCoherentSet() != container.GetCoherentSet()) {
			throw std::logic_error("Not in the same coherent set.");
		}
	}

	auto operator[](size_t index) const {
		return m_container[m_selection[index]];
	}

	size_t Size() const {
		return m_selection.Size();
	}
private:
	const SelectionSet& m_selection;
	const SelectableContainer<UnderlyingT>& m_container;
};





template <class Proxy, class... Containers>
class ContainerMerger : private std::tuple<Containers&...> {
public:
	ContainerMerger(Containers&... containers) : std::tuple<Containers&...>(containers...) {
		if (!IsAllInSameSet(containers...)) {
			throw std::logic_error("Not in same set");
		}
	}

	Proxy operator[](size_t index) {
		return MakeProxy<0>(index);
	}
private:
	template <size_t Count, size_t... Indices>
	Proxy MakeProxy(size_t index) {
		if constexpr (Count == sizeof...(Containers)) {
			return Proxy{std::get<Indices>(*this)[index]...};
		}
		else {
			return MakeProxy<Count+1, Indices..., Count>(index);
		}
	}

	template <class Head, class... Rest>
	bool IsAllInSameSet(const Head& head, const Rest&... containers) {
		return IsAllInSet(head.GetCoherentSet(), head, containers...);
	}

	bool IsAllInSet(CoherentSet set) {
		return true;
	}

	template <class Head, class... Rest>
	bool IsAllInSet(CoherentSet set, const Head& head, const Rest&... containers) {
		return set == head.GetCoherentSet() && IsAllInSet(set, containers...);
	}
};



struct VeloHit {
	std::reference_wrapper<float> x;
	std::reference_wrapper<float> y;
	std::reference_wrapper<float> z;
	std::reference_wrapper<float> errx;
	std::reference_wrapper<float> erry;
};



template <class T>
using SelectableVector = SelectableContainer<std::vector<T>>;



int main() {
	CoherentSet set;
	SelectableVector<float> x(set), y(set), z(set), errx(set), erry(set);

	for (int i=0; i<10; ++i) {
		x.push_back(i);
		y.push_back(2*i);
		z.push_back(3*i);
		errx.push_back(4*i);
		erry.push_back(5*i);
	}

	ContainerMerger<VeloHit, SelectableVector<float>, SelectableVector<float>, SelectableVector<float>, SelectableVector<float>, SelectableVector<float>> merger(x, y, z, errx, erry);
	float& hitx = merger[0].x;
	merger[0].x *= 2.0f;
	hitx = 6.0f;

	SelectionSet sel(x, [](float value) { return unsigned(value)%2 == 0; });

	SelectionView<decltype(x)> view(sel, x);
	for (size_t i=0; i<view.Size(); ++i) {
		std::cout << view[i] << " ";
	}
	std::cout << std::endl;
}